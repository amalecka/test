import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.*


def customField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject("customfield_10200")
def czasochlonnosc = issue.getCustomFieldValue(customField) as Integer
if (czasochlonnosc) {
    return czasochlonnosc * 150
}
else {
    return null
}